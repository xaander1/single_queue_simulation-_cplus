//Single Server Simulation
#include <iostream>
#include <string>
using namespace std;
int simStartH;
int simStartM;
const int MAX_ITEMS=20;
int customers[MAX_ITEMS];
int RNarr[20] = { 82,91,12,77,90,75,33,61,19,58,41,54,52,16,86,24,41,19,97,34};
int RNserv[20] = { 93,59,76,62,40,41,57,91,93,38,92,22,9,7,65,62,50,28,16,04};
int IAT[MAX_ITEMS];
int ST[MAX_ITEMS];
int StartTH[MAX_ITEMS];
int StartTM[MAX_ITEMS];
int ATCH[MAX_ITEMS];
int ATCM[MAX_ITEMS];
int ESH[MAX_ITEMS];
int ESM[MAX_ITEMS];
int NQ[MAX_ITEMS];
int SIT[MAX_ITEMS];
int Delay[MAX_ITEMS];
int numCust;

//declare a function
void addTime(int, int);
int calc(int);
int idlecalc(int);
int main(){
	cout << "Enter the number of customers you want to display simulation(Range 1-20):  ";
	cin >> numCust;
	cout << "Enter simulation start Hour(Range 1-12 e.g 12:30 hour is 12): ";
	cin >> simStartH;
	cout << "Enter simulation starting Minutes(Range 0-60 e.g 12:30 minute is 30): ";
	cin >> simStartM;
	const int simStartM2 = simStartM+0;
	const int simStartH2 = simStartH + 0;
	int removeCollision2;
	int removeCollision;
	cout << "CS\trnA\tIAT\tATC\trnS\tST\tSTT\tEST\tNQ\tSIT\tD"<< endl;
		for (int i = 0; i<numCust; i++){
			customers[i] = i + 1;
			if (RNarr[i] >= 00 && RNarr[i] <= 19) {
				IAT[i] = 1;
			}
			else if (RNarr[i] >= 20 && RNarr[i] <= 44) {
				IAT[i] = 2;
			}
			else if (RNarr[i] >= 45 && RNarr[i] <= 74) {
				IAT[i] = 3;
			}
			else if (RNarr[i] >= 75 && RNarr[i] <= 89) {
				IAT[i] = 4;
			}
			else if (RNarr[i] >= 90 && RNarr[i] <= 99) {
				IAT[i] = 5;
			}
			else {
				IAT[i] = 0;
			}

			if (RNserv[i] >= 0 && RNserv[i] <= 9) {
				ST[i] = 1;
			}
			else if (RNserv[i] >= 10 && RNserv[i] <= 24) {
				ST[i] = 2;
			}
			else if (RNserv[i] >= 25 && RNserv[i] <= 59) {
				ST[i] = 3;
			}
			else if (RNserv[i] >= 60 && RNserv[i] <= 74) {
				ST[i] = 4;
			}
			else if (RNserv[i] >= 75 && RNserv[i] <= 89) {
				ST[i] = 5;
			}
			else if (RNserv[i] >= 90 && RNserv[i] <= 99) {
				ST[i] = 6;
			}
			else{
				ST[i] = 0;
			}
			
			addTime(IAT[i],i);
			if (i == 0) {
				StartTM[i] = ATCM[i];
				StartTH[i] = ATCH[i];
				removeCollision2=ATCH[i]+0;
//end service
				ESM[i]=StartTM[i]+ST[i];
				removeCollision=ATCH[i]+0;
				ESH[i] = removeCollision;
				if (ESM[i] >= 60) {
					removeCollision += 1;
					ESH[i]=removeCollision;
					ESM[i] -= 60;
				}
				NQ[i] = 0;

				if (ATCH[i] == simStartH2) {
					SIT[i] = ATCM[i] - simStartM2;
				}
				else if (ATCH[i] > simStartH2) {
					int a, b;
					a = ATCM[i] + 60;
					b = simStartM2 + 0;
					SIT[i] = a - b;
				}else
				{
					int a, b;
					a = ATCM[i] + 0;
					b = simStartM2 + 60;
					SIT[i] = a - b;
					}
			}
			else {
				//deals with start time
				StartTM[i] = StartTM[i - 1] + ST[i - 1];
				StartTH[i] = removeCollision2;
				
				if (StartTM[i] >= 60) {
					removeCollision2+= 1;
					StartTH[i]=removeCollision2;

					StartTM[i] -= 60;
				}
				//deals with end time
				ESM[i] = ESM[i - 1] + ST[i];
				ESH[i] = removeCollision;
				if (ESM[i] >= 60) {
					removeCollision += 1;
					ESH[i]=removeCollision;
					ESM[i] =ESM[i]-60;
				}
				NQ[i]=calc(i);
				SIT[i] = idlecalc(i);
			}

			//delay
			if (StartTH[i] == ATCH[i]) {
				Delay[i] = StartTM[i] - ATCM[i];
			}
			else if (StartTH[i] > ATCH[i]) {
				int c, d;
				c = StartTM[i] + 60;
				d = ATCM[i] + 0;
				Delay[i] = c - d;
			}
			else
			{
				int c, d;
				c = StartTM[i] + 0;
				d = ATCM[i] + 60;
				Delay[i] = c - d;
			}
			
			//cout << StartTH[i] << ":" << StartTM[i] << "and" << ESH[i] << ":" << ESM[i] << endl;
			//cout << NQ[i] << endl;
			//cout << SIT[i] << endl;
			//cout << Delay[i] << endl;
			cout << customers[i] << "\t" << RNarr[i] << "\t" << IAT[i] << "\t" << ATCH[i] << ":" << ATCM[i] << "\t" << RNserv[i] <<
				"\t" << ST[i] << "\t" << StartTH[i] << ":" << StartTM[i] << "\t" << ESH[i]<<":"<<ESM[i]<<"\t"<<NQ[i]<<"\t"<<SIT[i]<<"\t"<<Delay[i]<<endl;
		}
	return 0;
}

void addTime(int min,int pos) {
	simStartM += min;
	if (simStartM >= 60) {
		simStartH +=1;
		simStartM = simStartM - 60;
	}
	ATCH[pos] = simStartH;
	ATCM[pos] = simStartM;
}
int calc(int pos) {
	int k = 0;
	int a, b;
	for (int j = 0; j < pos; j++)
	{
		if (ESH[j]==ATCH[pos]) {
			if (ESM[j] > ATCM[pos]) {
				k++;
		}
		}
		else if(ESH[j]> ATCH[pos])
		{
			a = ESM[j]+60;
			b = ATCM[pos] + 0;
			if (a > b) {
				k++;
			}
		}
		else
		{
			a = ESM[j] + 0;
			b = ATCM[pos] + 60;
			if (a > b) {
				k++;
			}
		}
		}
		return k;
	}

int idlecalc(int pos){
	int num;
	int a, b;
	if (ATCH[pos] == StartTH[pos]) {
		num = ATCM[pos] - StartTM[pos];
	if (num <= 0) {
			num = 0;
		}
	}
	else if (ATCH[pos]> StartTH[pos])
	{
		a = ATCM[pos] + 60;
		b = StartTM[pos] + 0;
		num = a - b;
		if (num <= 0) {
			num = 0;
		}
	}
	else
	{
		a = ATCM[pos] + 0;
		b = StartTM[pos] + 60;
		num = a - b;
		if (num <= 0) {
			num = 0;
		}
	}
		return num;
	}
//Written By: Alexander Njogu
//Multimedia University of Kenya
//https://gitlab.com/xaander1/single_queue_simulation-_cplus